<?php

// Отправляем браузеру правильную кодировку,
// файл login.php должен быть в кодировке UTF-8 без BOM.
header('Content-Type: text/html; charset=UTF-8');

// Начинаем сессию.
session_start();

// В суперглобальном массиве $_SESSION хранятся переменные сессии.
// Будем сохранять туда логин после успешной авторизации.
if (!empty($_SESSION['login'])) {
  // Если есть логин в сессии, то пользователь уже авторизован.
  // TODO: Сделать выход (окончание сессии вызовом session_destroy()
  //при нажатии на кнопку Выход).
  session_destroy();
  header('Location: ./');
}

// В суперглобальном массиве $_SERVER PHP сохраняет некторые заголовки запроса HTTP
// и другие сведения о клиненте и сервере, например метод текущего запроса $_SERVER['REQUEST_METHOD'].
if ($_SERVER['REQUEST_METHOD'] == 'GET') {
?>

<head>
    <title>SP25</title>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <!-- My CSS -->
    <link rel="stylesheet" href="css/style2.css">
</head>

<div class="custom container-fluid d-flex align-items-center justify-content-center">
    <div class="row bg-light scheduler-border">
        <div class="col-12 mt-5 col-xs-12 col-md-12 col-lg-12">

            <h1 style="text-align:center">Вход</h1>
            <form action=" " method="post">
                <?php 
                  if(!empty($_COOKIE['enter_error'])){
                    echo '<div class="alert-box errorr" style="text-align:center"> Неверный логин или пароль </div>';
                    setcookie('enter_error', '', 1000);
                  }
                ?>
                <!-- Login-->
                <div class="form-group mt-2">
                    <input class="form-control" name="login" placeholder="Login" />
                </div>
                <!-- Password -->
                <div class="form-group">
                    <input class="form-control" name="pass" placeholder="Password" />
                </div>
                <!-- Button -->

                <div class="col- mt-1 mb-5">
                    <button type="submit" class="btn btn-primary btn-block">Войти</button>
                </div>
            </form>
        </div>
    </div>
</div>
<?php
}
// Иначе, если запрос был методом POST, т.е. нужно сделать авторизацию с записью логина в сессию.
else {

  // TODO: Проверть есть ли такой логин и пароль в базе данных.
  // Выдать сообщение об ошибках.
  $user = 'u17333';
  $pass = '7038049';
  $db = new PDO('mysql:host=localhost;dbname=u17333', $user, $pass, array(PDO::ATTR_PERSISTENT => true));
  $stmt = $db->prepare('SELECT * FROM user WHERE login = ? AND password = ?;');
  $stmt->bindValue(1, $_POST['login'], PDO::PARAM_STR);
  $stmt->bindValue(2, md5($_POST['pass']), PDO::PARAM_STR);
  $stmt->execute();
  $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
  if(!empty($rows)){
  $_SESSION['login'] = $_POST['login'];
  $_SESSION['pass'] = $_POST['pass'];
  $_SESSION['uid'] = $rows[0]['id'];
  header('Location: registration.php');
  } 
  else 
  {
    setcookie('enter_error', 1, 0);
    header('Location: login.php');
  }
  
}
?>