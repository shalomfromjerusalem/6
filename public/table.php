<!DOCTYPE HTML>
<html>
  <head> 
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="css/adm_style.css">
  </head>
</html>
<?php function printElems(Array $a){ ?> 
  <tr> 
      <?php
      $abilitiesTranslation =[
        'Immortal' => 'Бессмертие',
        'Walk' => 'Прохождение сквозь стены',
        'Levitation' => 'Левитация',
        'Telekinez' => 'Телекинез'
      ];      
      $id = $a['id'];
      foreach($a as $key => $value){
        if($key == 'superpowers'){
          $abilities =  json_decode($a['superpowers']);
      ?> 
  <td> 
     <?php
      foreach( $abilities as $ability){
        echo !empty($abilitiesTranslation[$ability]) ? $abilitiesTranslation[$ability] . ' '  : '';
      }
     ?> 
  </td> 
      <?php
        } else if($key != 'id') {
      ?> 
  <td> <?=strip_tags($value)?> </td> 
      <?php
        }
      }
      ?>
  <td> 
    <form action="admin.php" method="POST">
    <button class="btn btn-outline-dark" name="id" type = "submit" value="<?=$id?> "> Удалить</button>
    </form>
  </td>
</tr>
   <?php
}
$user = 'u17333';
$pass = '7038049';
$db = new PDO('mysql:host=localhost;dbname=u17333',$user, $pass, array(PDO::ATTR_PERSISTENT => true));
$stmt = $db->prepare('SELECT id, name, email, birthday, sex, limb, superpowers, bio FROM user');
$stmt->execute();
$rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
  ?> 

<div class="col-12 col-lg-7 mx-lg-auto order-lg-1 order-sm-2">
    <h2>Панель администратора</h2>
    <p>Таблица пользователей</p>
    <table class="table table-bordered table-hover">
      <thead class="thead-dark">
        <tr>
            <th>Имя</th>
            <th>Email</th>
            <th>Дата рождения</th>
            <th>Пол</th>
            <th>Количество конечностей</th>
            <th>Сверхспособности</th>
            <th>Биография</th>
            <th></th>
        </tr>
      </thead>
    <?php    
foreach($rows as $row){
  printElems($row);
}
    ?> 
    </table> 
  </div>